function PromedioCalificacionMayorYMenor(){
  var calificacion=document.getElementById('tabla');
  var tabla_body=calificacion.lastElementChild;
  var ul=document.getElementById('lista');
  var tabla=tabla_body.children.length-1;
  var total=0;
  var base = Number(tabla_body.children[1].children[2].textContent);
  var calimenor;
  /*obtenemos el promedio*/
  for (var i=1; i<tabla_body.children.length; i++){
    total += Number(tabla_body.children[i].children[2].textContent);
  }
  var promedio=total/tabla;
  var listaPromedio=ul.children[0].textContent;
  ul.children[0].textContent=listaPromedio+promedio;
  
  /*obtenemos la clave y nombre de la materia con mayor calificacion*/
  for (var i=2; i<tabla_body.children.length; i++){
    calimayor = Number(tabla_body.children[i].children[2].textContent);
       if (calimayor > base){
          base  = Number(tabla_body.children[i].children[2].textContent);
          var clave = tabla_body.children[i].children[0].textContent;
          var materia = tabla_body.children[i].children[1].textContent;
          var listaMayor=ul.children[1].textContent;
          ul.children[1].textContent=listaMayor+ " ("+clave+ ")" +"  "+materia;
        }
    }
  
   /*obtenemos la clave y nombre de la materia con menor calificacion*/
  for (var i=2; i<tabla_body.children.length; i++){
    calimenor = Number(tabla_body.children[i].children[2].textContent);
       if (calimenor < base){
          base = Number(tabla_body.children[i].children[2].textContent);
          var clave1 = tabla_body.children[i].children[0].textContent;
          var materia1 = tabla_body.children[i].children[1].textContent;
          var listaMenor=ul.children[2].textContent; 
         ul.children[2].textContent=listaMenor+ " ("+clave1+ ")" +"  "+materia1;         
        }
  }
}